var GLOBAL = this;
if (typeof path_resource == "undefined") path_resource = "";
if (typeof basePath == 'undefined') basePath = '';
if (typeof path_resource_domain == 'undefined') path_resource_domain = '';

Object.defineProperties(GLOBAL, {
    isTestingLocal: {
        get: function () {
            return window.location.hostname.includes('localhost')
                || window.location.hostname.includes('192.168')
                || window.location.origin.includes('file')
                || window.location.href.includes("https://dev4.digitop.vn/demo")
                ;
        }
    },
});


var globalCssList = [
    // main CSS files:
    // path_resource + "css/main.css",
    // path_resource + "css/pure.css",

    // // Helper CSS file: preloader & some quick styling css
    // path_resource + "css/helper.css",

    // More CSS file to inject? Use this:
    // path_resource + "css/test.css", 
];

var globalJsList = [
    // libraries
    isTestingLocal ? path_resource + 'js/libraries/jquery.min.js' : '',
    // plugins
    path_resource + 'js/libraries/bundle.js',
    //bundle include:
    //js/plugins/digitop/lazyload.js 
    //js/plugins/tweenmax/TweenMax.min.js 
    //js/plugins/digitop/helper.js 
    //js/plugins/digitop/popup.js 
    //js/plugins/digitop/preloader.js 

    isTestingLocal ? path_resource + 'js/plugins/sweetalert2/sweetalert2.min.js' : '',
    isTestingLocal ? path_resource + 'js/plugins/sweetalert2/sweetalert2.min.css' : '',

    // common scripts will execute in all templates:
    path_resource + 'js/modules/common.js',
];

/* GLoader - version 1.2
- Description: preload external Image, JS & CSS files
- Date: Aug 12, 2017
- Author: Goon Nguyen
================================================== */
var GLoader = {
    version: '1.4.2',

    // Load script: JS or CSS
    loadScript: function (url, callback) {

        if (url.isEmpty()) if (callback) callback();

        var done = false;
        var fileType = url.indexOf('.js') > -1 ? 'js' : 'css';
        var result = { status: false, message: '' };
        var script = fileType == 'js'
            ? document.createElement('script')
            : document.createElement('link');

        script.setAttribute('data-loader', 'GLoader');
        if (fileType == 'js') {
            // script.setAttribute('async', "");
            script.setAttribute('type', 'text/javascript');
            embedJsToHTML();
        } else {
            script.setAttribute('rel', 'stylesheet');
            script.setAttribute('type', 'text/css');
            script.setAttribute('href', url);
        }

        function embedJsToHTML() {

            GLoader.loadFile(url, {
                onComplete: onLoad,
                onError: handleError
            });

            function onLoad(content) {
                GLoader.embedScript(script, GLoader.getFileName(url), content)
                // script.innerHTML = content;
                if (callback) callback(result);
            }
        };

        // // events
        // script.onload = handleLoad;
        // script.onreadystatechange = handleReadyStateChange;
        // script.onerror = handleError;

        if (fileType == 'js') {
            document.body.appendChild(script);
        } else {
            document.head.appendChild(script);

            script.onload = handleLoad;
            script.onreadystatechange = handleReadyStateChange;
            script.onerror = handleError;
        }

        function handleLoad() {
            if (!done) {
                done = true;

                result.status = true;
                result.message = 'Script was loaded successfully';

                if (callback) callback(result);
            }
        }

        function handleReadyStateChange() {
            var state;

            if (!done) {
                state = script.readyState;
                if (state === 'complete') {
                    handleLoad();
                }
            }
        }

        function handleError() {
            //console.log("error");
            if (!done) {
                done = true;
                result.status = false;
                result.message = 'Failed to load script.';
                if (callback) callback(result);
            }
        }
    },

    // check file existed:
    isExisted: function (filePath) {
        var fileName = this.getFileName(filePath);
        var _name = "js_" + fileName;

        if (document.getElementsByName(_name).length != 0) return true;

        var scripts = document.getElementsByTagName('script');
        var existed = false;
        for (var i = 0; i < scripts.length; i++) {
            if (scripts[i].src) {
                var src = scripts[i].src;
                // console.log(filePath);
                if (
                    String(src)
                        .toLowerCase()
                        .indexOf(fileName.toLowerCase()) >= 0
                ) {
                    existed = true;
                }
                //console.log(i,scripts[i].src)
            } else {
                //console.log(i,scripts[i].innerHTML)
            }
        }
        return existed;
    },

    embedScript: function (node, nameJs, data) {
        // node
        var _name = node.getAttribute('name');

        if (_name.isEmpty()) {
            var _name = "js_" + nameJs;
            node.setAttribute('name', _name);
        }

        node.textContent = data;
    },

    // Load list of scripts:
    loadScripts: function (array, options) {

        var options = options || {};

        var onComplete = options['onComplete'] || null
            , onProcess = options['onProcess'] || null
            ;

        if (array.length == 0) {
            if (onComplete) onComplete();
            return;
        }

        var listCss = array.filter(function (url) {
            return url.indexOf('.css') > -1
        }).map(function (url) {
            return GLoader.loadScript(url);
        })

        var listJs = [];
        var list = [];
        var index = 0;

        for (var i = 0; i < array.length; i++) {

            var url = array[i];

            if (url.indexOf('.js') <= -1) continue;

            var _name = "js_" + GLoader.getFileName(url);

            if (GLoader.isExisted(GLoader.getFileName(url))
            ) continue;


            var script = document.createElement('script');
            script.setAttribute('name', _name);
            script.setAttribute('data-loader', 'GLoader');
            script.setAttribute('type', 'text/javascript');
            document.body.appendChild(script);

            listJs.push(array[i]);

            list.push({
                id: index,
                url: url,
                name: _name,
            })

            index++;
        }

        var idAdded = -1;

        var listLoaded = [];

        this.loadMultiFile(listJs, {
            maxQueue: 7,
            onProgress: function (process, _url, data) {
                // console.log(process, _url);

                var itemLoaded = list.find(function (_item) {
                    return _item.url == _url;
                })

                itemLoaded.data = data;
                listLoaded.push(itemLoaded);
                checkEmbed(itemLoaded);

                if (onProcess != null) onProcess(process);

            },
            onComplete: function () {
                if (onComplete) onComplete();

                // console.log('complete');
            }

        })


        function checkEmbed(itemLoaded) {

            var idNext = listLoaded.find(function (_item) {
                return _item.id == idAdded + 1
            })

            if (idNext) {
                listLoaded.sort(function (a, b) {
                    return a.id - b.id;
                });

                embed();
            }
        }


        function embed() {
            for (var i = 0; i < listLoaded.length; i++) {
                var item = listLoaded[i];
                // console.log('item.id', item.id, idAdded);
                if (item.id != idAdded + 1) return;

                idAdded++;

                // document.getElementsByName(item.name)[0].innerHTML = item.data;
                GLoader.embedScript(document.getElementsByName(item.name)[0], item.name, item.data)

                listLoaded.shift();
                i--;

            }
        }

        // var result = { status: false, message: '' };
        // var count = 0;
        // var total = array.length;
        // //console.log("loadScripts")
        // this.loadScript(array[count], onComplete);

        // function onComplete(result) {
        // 	count++;
        // 	// console.log(count, total)
        // 	if (count == total) {
        // 		result.status = true;
        // 		result.message = 'All scripts were loaded.';
        // 		if (callback) callback(result);
        // 	} else {
        // 		if (GLoader.isExisted(array[count])) {
        // 			console.log(
        // 				'[GLoader] The script "' +
        // 				array[count] +
        // 				'" was existed -> Skipped.'
        // 			);
        // 			onComplete();
        // 		} else {
        // 			GLoader.loadScript(array[count], onComplete);
        // 		}
        // 	}
        // }
    },

    // load single photos
    // url: String, options: Object
    loadPhoto: function (url, options, callback) {
        var img = new Image();
        img.onload = function () {
            if (typeof callback != 'undefined') callback(url);
        };
        img.onerror = function () {
            if (typeof callback != 'undefined') callback(null);
        };
        img.src = url;
    },

    // load multiple photos
    // urls: Array, options: Object
    loadPhotos: function (urls, options, callback) {
        var array = urls;
        var count = 0;
        var total = array.length;
        var result = { status: false, message: '' };
        var photos = [];

        var currentURL = array[count];
        this.loadPhoto(currentURL, options, onComplete);

        function onComplete(url) {
            count++;
            //console.log(count, total)
            if (count == total) {
                result.status = true;
                result.message = 'All photos were loaded.';
                result.photos = photos;
                if (callback) callback(result);
            } else {
                photos.push(url);
                currentURL = array[count];
                GLoader.loadPhoto(currentURL, options, onComplete);
            }
        }
    },

    loadFile: function (url, options) {

        options = options != undefined ? options : {};

        var onProgress = options.hasOwnProperty("onProgress") ? options['onProgress'] : null;
        var onComplete = options.hasOwnProperty("onComplete") ? options['onComplete'] : null;
        var onError = options.hasOwnProperty("onError") ? options['onError'] : null;
        var responseType = options.hasOwnProperty("responseType") ? options['responseType'] : 'text';

        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.responseType = responseType;

        req.onprogress = function (e) {
            var progress = e.loaded / e.total;
            downloaded = e.loaded;
            if (onProgress != null) onProgress(progress);
        };

        req.onreadystatechange = function () {

            if (req.readyState == 2) {
                // response headers received
            }

            if (req.readyState == 3) {
                // loading
            }
            if (req.readyState == 4) {
                // request finished
            }
        };

        req.onload = function () {

            if (req.readyState === req.DONE) {
                if (req.status === 200) {
                    // console.log(req.response);
                    if (onComplete != null) onComplete(req.response, url);
                    return;
                }
            }

            if (onError != null) onError('Status error ' + this.status);

        };

        req.onerror = function (err) {
            // Error
            console.log(err);
            if (onError != null) onError('Loading error');
        };

        req.send();
    },

    loadMultiFile: function (urls, options) {

        options = options != undefined ? options : {};

        var onProgress = options.hasOwnProperty("onProgress") ? options['onProgress'] : null;
        var onComplete = options.hasOwnProperty("onComplete") ? options['onComplete'] : null;
        var onError = options.hasOwnProperty("onError") ? options['onError'] : null;

        var maxQueue = options.hasOwnProperty("maxQueue") ? options['maxQueue'] : 5;

        var responseType = options.hasOwnProperty("responseType") ? options['responseType'] : 'text';

        var currentQueueCount = 0
            , currentIdLoading = 0
            , idComplete = 0;

        if (urls.length == 0 || urls == null) {
            onComplete();
        }

        if (urls.length < maxQueue) {
            //length < maxQueue
            for (let i = 0; i < urls.length; i++) {
                var url = urls[i];

                this.loadFile(url,
                    {
                        // onProgress: _onProgress,
                        responseType: responseType,
                        onError: _onError,
                        onComplete: function (data, _url) {
                            // console.log(data);
                            _onProgress(_url, data);

                            if (idComplete >= urls.length) {
                                _onComplete();
                            }
                        },
                    });
            }
        } else {
            //load per [maxQueue] each
            loadedInQueue();
        }

        function loadedInQueue() {
            //
            if (currentIdLoading < urls.length) {

                if (currentQueueCount < maxQueue) {

                    currentQueueCount++;

                    var urlLoading = urls[currentIdLoading];
                    currentIdLoading++;

                    GLoader.loadFile(urlLoading,
                        {
                            responseType: responseType,
                            onComplete: function (data, _url) {

                                currentQueueCount--;

                                _onProgress(_url, data);

                                if (currentIdLoading >= urls.length && currentQueueCount == 0) {
                                    _onComplete();
                                } else {
                                    loadedInQueue();
                                };
                            },
                            // onProgress: _onProgress,
                            onError: _onError,
                        });

                    loadedInQueue();
                }
            }
        }


        function _onProgress(url, data) {
            idComplete++;
            var precent = (idComplete) / (urls.length);

            if (onProgress) onProgress(precent, url, data);
        }

        function _onError(errorString) {
            if (onError) onError(errorString);
        }

        function _onComplete() {
            // console.log("complete");
            if (onComplete) onComplete();
        }


    },


    getFileName: function (path) {
        if (path.indexOf('?') > -1) {
            path = path.substring(0, path.indexOf('?'));
        }

        var startIndex = (path.indexOf('\\') >= 0 ? path.lastIndexOf('\\') : path.lastIndexOf('/'));
        var filename = path.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
        return filename
    },


};

var MAIN = {
    init: function () {
        // Inject JS and CSS files:
        // These scripts will be available in every pages.
        var scriptArr = [];
        globalCssList.forEach(function (css) {
            scriptArr.push(css);
        });
        globalJsList.forEach(function (js) {
            scriptArr.push(js);
        });

        var mainTag = document.getElementsByTagName('main')[0];
        if (!mainTag) {
            console.log('[MAIN] Thẻ <main> trong HTML của bạn đâu?');
            return;
        }

        var pageID = mainTag.getAttribute('id');
        if (!pageID) {
            console.log(
                "[MAIN] Thẻ <main> hình như chưa có 'id' kìa. Mà nếu không cần thì thôi cũng chả sao!"
            );
        } else {
            scriptArr.push(path_resource + 'js/pages/' + pageID + '.js' + randomVersion());
        }

        GLoader.loadScripts(scriptArr, {
            onComplete: function (result) {
                // inject page script (if any):
                if (pageID) {
                    if (GLOBAL[pageID] && typeof GLOBAL[pageID].init != 'undefined') {
                        GLOBAL[pageID].init();
                    } else {
                        console.log(
                            '[MAIN] Không tìm thấy Class [' + pageID + '] nào để gọi init() cả.'
                        );
                    }
                } else {
                    console.log(
                        '[MAIN] Chả có Class JS (id) nào cho trang này để load cả.'
                    );
                }
            }
        });
        // End MAIN init.
    }
};



//	Polyfills

/*

Object
    prototype:
        randomValue
        randomKey
        randomElementIndex
        getObjectLength

Array
    prototype:
        first
        last
        randomIndex
        randomElement

    method:
        getRandom
        getHalfRandom
        shuffle
        moveIndex

String
    prototype:
        isEmpty

*/

//Object.assign
"function" != typeof Object.assign && Object.defineProperty(Object, "assign", { value: function (e, t) { "use strict"; if (null == e) throw new TypeError("Cannot convert undefined or null to object"); for (var n = Object(e), r = 1; r < arguments.length; r++) { var o = arguments[r]; if (null != o) for (var c in o) Object.prototype.hasOwnProperty.call(o, c) && (n[c] = o[c]) } return n }, writable: !0, configurable: !0 });

//String.includes
String.prototype.includes || (String.prototype.includes = function (t, e) { "use strict"; return "number" != typeof e && (e = 0), !(e + t.length > this.length) && -1 !== this.indexOf(t, e) });

//Array.find
Array.prototype.find || Object.defineProperty(Array.prototype, "find", { value: function (r) { if (null == this) throw TypeError('"this" is null or not defined'); var t = Object(this), e = t.length >>> 0; if ("function" != typeof r) throw TypeError("predicate must be a function"); for (var i = arguments[1], o = 0; o < e;) { var n = t[o]; if (r.call(i, n, o, t)) return n; o++ } }, configurable: !0, writable: !0 });



Object.defineProperties(Object.prototype, {

    randomValue: {
        get: function () {
            return this[this.randomKey];
        }
    },
    randomKey: {
        get: function () {
            var keys = Object.keys(this);
            return keys[keys.length * Math.random() << 0];
        }
    },
    randomElementIndex: {
        get: function () {
            var keys = Object.keys(this)
            return keys.length * Math.random() << 0;
        }
    },
    getObjectLength: {
        get: function () {
            var keys = Object.keys(this)
            return keys;
        }
    }

})



Object.defineProperties(Array.prototype, {

    first: { get: function () { return this[0]; }, },

    last: { get: function () { return this[this.length - 1]; }, },

    randomIndex: { get: function () { return Math.floor(Math.random() * this.length); }, },

    randomElement: { get: function () { return this[Math.floor(Math.random() * this.length)]; }, },

});





Object.assign(Array.prototype, {

    getRandom: function (n) {
        var result = new Array(n),
            len = this.length,
            taken = new Array(len);
        if (n > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (n--) {
            var x = Math.floor(Math.random() * len);
            result[n] = this[x in taken ? taken[x] : x];
            taken[x] = --len in taken ? taken[len] : len;
        }
        return result;
    },

    getHalfRandom: function () {
        var n = Math.ceil(this.length / 2);
        return this.getRandom(n);
    },


    shuffle: function () {
        var i = this.length, j, temp;
        if (i == 0) return this;
        while (--i) {
            j = Math.floor(Math.random() * (i + 1));
            temp = this[i];
            this[i] = this[j];
            this[j] = temp;
        }
        return this;
    },

    moveIndex: function (oldIndex, newIndex) {
        // return Math.floor(Math.random() * this.length);
        if (newIndex >= this.length) {
            var k = newIndex - this.length + 1;
            while (k--) {
                this.push(undefined);
            }
        }
        this.splice(newIndex, 0, this.splice(oldIndex, 1)[0]);
        return this; // for testing
    },
})



Object.assign(String.prototype, {

    isEmpty: function () { return this === null || this.match(/^ *$/) !== null; },

})




function randomVersion() {
    return '?v=' + (+new Date()) + (9999 * Math.random() << 0);
}


// Execute INIT:
MAIN.init();
























